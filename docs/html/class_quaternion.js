var class_quaternion =
[
    [ "Quaternion", "class_quaternion.html#abcc01358aada56ea5f0db4da18aaf77d", null ],
    [ "Quaternion", "class_quaternion.html#a2298a8d1d3c61791991833db8ca32bb8", null ],
    [ "addScaledVector", "class_quaternion.html#a9b6aa114c5bfe9e849b0f601428b971b", null ],
    [ "normalize", "class_quaternion.html#a48e6bafb249c895ed25f29dc9170e11e", null ],
    [ "operator*=", "class_quaternion.html#aae0eb1177a8fb6b765a6ab82d3f7ccc4", null ],
    [ "rotateByVector", "class_quaternion.html#a0a9dbdee0efafed4f02f8cacfd8bc9f1", null ],
    [ "data", "class_quaternion.html#a58f70bd4ca88be9aca2e274ce8ae40e3", null ],
    [ "i", "class_quaternion.html#add0a0d14a637f6c201fe5d1083635c59", null ],
    [ "j", "class_quaternion.html#a278770e721d4bef78799780c2fa751fb", null ],
    [ "k", "class_quaternion.html#aba5007145fef6f1a707026d679f6e4cd", null ],
    [ "r", "class_quaternion.html#aecaa2cd74473fd2c8f3399080e6c621e", null ]
];