var hierarchy =
[
    [ "BBox", "class_b_box.html", null ],
    [ "Camera", "class_camera.html", null ],
    [ "CollisionData", "class_collision_data.html", null ],
    [ "CollisionDetector", "class_collision_detector.html", null ],
    [ "CollisionPrimitive", "class_collision_primitive.html", [
      [ "CollisionBox", "class_collision_box.html", null ],
      [ "CollisionPlane", "class_collision_plane.html", null ],
      [ "CollisionSphere", "class_collision_sphere.html", null ]
    ] ],
    [ "Color", "class_color.html", null ],
    [ "ConfigurationViewController()", "category_configuration_view_controller_07_08.html", null ],
    [ "Contact", "class_contact.html", null ],
    [ "ContactResolver", "class_contact_resolver.html", null ],
    [ "Controller", "class_controller.html", null ],
    [ "ForceGenerator", "class_force_generator.html", [
      [ "Gravity", "class_gravity.html", null ],
      [ "Spring", "class_spring.html", null ]
    ] ],
    [ "ForceRegistry::ForceRegistration", "struct_force_registry_1_1_force_registration.html", null ],
    [ "ForceRegistry", "class_force_registry.html", null ],
    [ "GLKViewController", null, [
      [ "SimulationViewController", "interface_simulation_view_controller.html", null ],
      [ "TestViewController", "interface_test_view_controller.html", null ]
    ] ],
    [ "ListObjectViewController()", "category_list_object_view_controller_07_08.html", null ],
    [ "MainCollision", "class_main_collision.html", null ],
    [ "MainEngine", "class_main_engine.html", null ],
    [ "MainGraphic", "class_main_graphic.html", null ],
    [ "MainPhysics", "class_main_physics.html", null ],
    [ "Matrix3", "class_matrix3.html", null ],
    [ "Matrix4", "class_matrix4.html", null ],
    [ "Matrix4x4", "class_matrix4x4.html", null ],
    [ "NDC", "class_n_d_c.html", null ],
    [ "Octree", "class_octree.html", null ],
    [ "OctreeNode", "struct_octree_node.html", null ],
    [ "Particle", "class_particle.html", null ],
    [ "ParticleContact", "class_particle_contact.html", null ],
    [ "ParticleContactGenerator", "class_particle_contact_generator.html", [
      [ "ParticleConstraint", "class_particle_constraint.html", [
        [ "ParticleCableConstraint", "class_particle_cable_constraint.html", null ],
        [ "ParticleRodConstraint", "class_particle_rod_constraint.html", null ]
      ] ],
      [ "ParticleLink", "class_particle_link.html", [
        [ "ParticleCable", "class_particle_cable.html", null ],
        [ "ParticleRod", "class_particle_rod.html", null ]
      ] ]
    ] ],
    [ "ParticleContactResolver", "class_particle_contact_resolver.html", null ],
    [ "ParticleForceGenerator", "class_particle_force_generator.html", [
      [ "ParticleAnchoredBungee", "class_particle_anchored_bungee.html", null ],
      [ "ParticleAnchoredSpring", "class_particle_anchored_spring.html", null ],
      [ "ParticleBungee", "class_particle_bungee.html", null ],
      [ "ParticleBuoyancy", "class_particle_buoyancy.html", null ],
      [ "ParticleDrag", "class_particle_drag.html", null ],
      [ "ParticleFakeStiffSpring", "class_particle_fake_stiff_spring.html", null ],
      [ "ParticleGravity", "class_particle_gravity.html", null ],
      [ "ParticleSpring", "class_particle_spring.html", null ]
    ] ],
    [ "ParticleForceRegistry::ParticleForceRegistration", "struct_particle_force_registry_1_1_particle_force_registration.html", null ],
    [ "ParticleForceRegistry", "class_particle_force_registry.html", null ],
    [ "PresentationViewController()", "category_presentation_view_controller_07_08.html", null ],
    [ "QuadTree", "class_quad_tree.html", null ],
    [ "QuadTreeNode", "struct_quad_tree_node.html", null ],
    [ "Quaternion", "class_quaternion.html", null ],
    [ "RigidBody", "class_rigid_body.html", [
      [ "SimulatedObject", "class_simulated_object.html", null ]
    ] ],
    [ "SaveSimulationViewController()", "category_save_simulation_view_controller_07_08.html", null ],
    [ "Selection", "class_selection.html", null ],
    [ "Shader", "class_shader.html", null ],
    [ "SimulationViewController()", "category_simulation_view_controller_07_08.html", null ],
    [ "TestController", "class_test_controller.html", null ],
    [ "TestViewController()", "category_test_view_controller_07_08.html", null ],
    [ "TorqueGenerator", "class_torque_generator.html", null ],
    [ "<UIApplicationDelegate>", null, [
      [ "AppDelegate", "interface_app_delegate.html", null ],
      [ "AppDelegate", "interface_app_delegate.html", null ]
    ] ],
    [ "UIResponder", null, [
      [ "AppDelegate", "interface_app_delegate.html", null ],
      [ "AppDelegate", "interface_app_delegate.html", null ]
    ] ],
    [ "UITabBarController", null, [
      [ "TabBarViewController", "interface_tab_bar_view_controller.html", null ]
    ] ],
    [ "<UITableViewDataSource>", null, [
      [ "ListObjectViewController", "interface_list_object_view_controller.html", null ],
      [ "SaveSimulationViewController", "interface_save_simulation_view_controller.html", null ]
    ] ],
    [ "<UITableViewDelegate>", null, [
      [ "ListObjectViewController", "interface_list_object_view_controller.html", null ],
      [ "SaveSimulationViewController", "interface_save_simulation_view_controller.html", null ]
    ] ],
    [ "UIViewController", null, [
      [ "ConfigurationViewController", "interface_configuration_view_controller.html", null ],
      [ "ListObjectViewController", "interface_list_object_view_controller.html", null ],
      [ "ObjectsViewController", "interface_objects_view_controller.html", null ],
      [ "PresentationViewController", "interface_presentation_view_controller.html", null ],
      [ "SaveSimulationViewController", "interface_save_simulation_view_controller.html", null ]
    ] ],
    [ "Vector3", "class_vector3.html", null ],
    [ "World", "class_world.html", null ]
];