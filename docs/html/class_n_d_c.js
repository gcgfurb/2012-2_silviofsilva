var class_n_d_c =
[
    [ "NDC", "class_n_d_c.html#a1a27ec4c73ee76de24ebb165b85a67c3", null ],
    [ "~NDC", "class_n_d_c.html#aaf87effd465c5c9eadc3c91029c17a0a", null ],
    [ "calcNDCCoordinates", "class_n_d_c.html#a40b1a7f20410ed6b5c98d02861f5f836", null ],
    [ "getAspect", "class_n_d_c.html#a4ca349132b8013c44638186cdc1b9920", null ],
    [ "getBottom", "class_n_d_c.html#a2fb718be0524fbe7c8749ee7d027d383", null ],
    [ "getLeft", "class_n_d_c.html#a98f76f4716315b172ddd3b499221fb2d", null ],
    [ "getMaxWindow", "class_n_d_c.html#a1c4f7bf0b787afb58eee45a17c45c126", null ],
    [ "getRight", "class_n_d_c.html#ad3ea66ef691a556ead8605ecde67a7cb", null ],
    [ "getTop", "class_n_d_c.html#a67ba293b982b8e000881801a35e1bbb0", null ],
    [ "setBottom", "class_n_d_c.html#aa418317ec368430bd7bf97b7df44bf59", null ],
    [ "setLeft", "class_n_d_c.html#ac1c49da13b50154d0adb52558485ac14", null ],
    [ "setRight", "class_n_d_c.html#a3cf516ed3e19fe048ad610110177d7af", null ],
    [ "setTop", "class_n_d_c.html#a0162571ffe4e78495d3fc08f6b1e1b9d", null ],
    [ "update", "class_n_d_c.html#ae024086511d6c880a8b626cf3c9958a3", null ]
];