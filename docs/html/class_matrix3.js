var class_matrix3 =
[
    [ "Matrix3", "class_matrix3.html#a773fdcf139826ddb39c30e7d08bbdb90", null ],
    [ "Matrix3", "class_matrix3.html#aa7dd16b4a84fe82cbadb4168964af664", null ],
    [ "inverse", "class_matrix3.html#a87d39a4a06b8309ec2a3ae97430dc2ca", null ],
    [ "invert", "class_matrix3.html#ac65c399c2ecb18c7abe099030198e700", null ],
    [ "operator*", "class_matrix3.html#a38d791267d03f9c0ade1024353f7e16c", null ],
    [ "operator*", "class_matrix3.html#a6c34599484fa823c775be73fc77c1487", null ],
    [ "operator*=", "class_matrix3.html#a7de52cf313977104ad75a9ed41eab08f", null ],
    [ "operator*=", "class_matrix3.html#aeb72f1ad45a071df9f639df9f7a50b5e", null ],
    [ "operator+=", "class_matrix3.html#a8d6d26c9eec75e4a64223c0ae3c8ccc4", null ],
    [ "setBlockInertiaTensor", "class_matrix3.html#a9a35a0bc6bebbddcd963ef24b875be3a", null ],
    [ "setComponents", "class_matrix3.html#a9a4cf4f01ec45a0dcf271f56103b3566", null ],
    [ "setInertiaTensorCoeffs", "class_matrix3.html#a8d2eb8b4e7079776a0ace8017aef9ce1", null ],
    [ "setInverse", "class_matrix3.html#ab68dac120706079a4a28a93dd3d45803", null ],
    [ "setOrientation", "class_matrix3.html#a03c8096eca68c135bfa93721ffd5d0e8", null ],
    [ "setSkewSymmetric", "class_matrix3.html#a6e689d5a7268f85a1c677862676dbc90", null ],
    [ "setTranspose", "class_matrix3.html#accd207f227ab901d5dcd5ad0c22d2f1f", null ],
    [ "transform", "class_matrix3.html#a000574fdcc420efae49ce73f857e9c73", null ],
    [ "transformTranspose", "class_matrix3.html#a745c765002a68bab4945286035eca74d", null ],
    [ "transpose", "class_matrix3.html#a7e02642cba051e4eddee67146fcc0d58", null ],
    [ "data", "class_matrix3.html#a375d020793ee0caaa3a6f9a0e969a794", null ]
];