var interface_objects_view_controller =
[
    [ "actionCircle:", "interface_objects_view_controller.html#ab54ea6d637b61645944faed4c954bc6c", null ],
    [ "actionClearSimulation:", "interface_objects_view_controller.html#afd17301d623e600726ab85dc78e71642", null ],
    [ "actionEditSimulation:", "interface_objects_view_controller.html#aae5ec8c51cc57b7a1cb784fc58c7d41f", null ],
    [ "actionGenerateRandomSimulation:", "interface_objects_view_controller.html#a119058f686d36877e9ab420fceda5e2a", null ],
    [ "actionSaveSimulation:", "interface_objects_view_controller.html#a619adf9e82cf0c5a535985e504df7342", null ],
    [ "actionSquare:", "interface_objects_view_controller.html#ab840aedebd1c79b64c40a2b251ab9511", null ],
    [ "actionStartSimulation:", "interface_objects_view_controller.html#a7cd3a315c35b19a84a5c6f83a7db5dde", null ],
    [ "actionSurfaceScene:", "interface_objects_view_controller.html#a7b07e9baede44d207008cb53ef5b271a", null ],
    [ "actionTriangle:", "interface_objects_view_controller.html#a1ea13c3f1581274312356ed3b923ab32", null ]
];