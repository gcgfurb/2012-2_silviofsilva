var searchData=
[
  ['can_5fawake',['CAN_AWAKE',['../d8/de2/_defines_8h.html#a18a15f0fee59b42fa36357fa35a47bd1',1,'Defines.h']]],
  ['channel_5fcolor',['CHANNEL_COLOR',['../d8/de2/_defines_8h.html#a11f24cd92d54eb6737aa9f367ac04114',1,'Defines.h']]],
  ['coef_5fangular_5fdamping',['COEF_ANGULAR_DAMPING',['../d8/de2/_defines_8h.html#adde89d9fcc54edfe5c1bbaeaf0ac275f',1,'Defines.h']]],
  ['coef_5ffriction',['COEF_FRICTION',['../d8/de2/_defines_8h.html#ad9093dfffd852e961abac5d5968c15af',1,'Defines.h']]],
  ['coef_5flinear_5fdamping',['COEF_LINEAR_DAMPING',['../d8/de2/_defines_8h.html#a19aad92851a4aaa3046209578ba87210',1,'Defines.h']]],
  ['coef_5frestitution',['COEF_RESTITUTION',['../d8/de2/_defines_8h.html#aaf56eb3f50ff550694a683d8277fb6ed',1,'Defines.h']]],
  ['count_5fcoord',['COUNT_COORD',['../d8/de2/_defines_8h.html#a882e7ab0f854b607e45428a5c132a7ee',1,'Defines.h']]]
];
