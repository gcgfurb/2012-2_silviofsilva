var searchData=
[
  ['init',['init',['../d6/d6c/class_rigid_body.html#a2cd14eee773ab0f8c1e7382b628f67ca',1,'RigidBody']]],
  ['initialize',['initialize',['../d7/d6c/class_simulated_object.html#a8ceee07be1b5f07c25e421f4100228d3',1,'SimulatedObject']]],
  ['initializecontextopengles',['initializeContextOpenGLES',['../d8/d87/class_controller.html#a0e66bd6d2261fcc90c00fcb97712bf68',1,'Controller::initializeContextOpenGLES()'],['../d7/d4a/class_test_controller.html#a5f06c9ea86624dd6166e71b56071320c',1,'TestController::initializeContextOpenGLES()']]],
  ['initializeengine',['initializeEngine',['../d8/d87/class_controller.html#a99a5ab77ea7678815e1d7610cf2746a5',1,'Controller::initializeEngine()'],['../d7/d4a/class_test_controller.html#a4bd32cbec6c0ef7992398409554d2d38',1,'TestController::initializeEngine()']]],
  ['initializegesturerecognizer_3a',['initializeGestureRecognizer:',['../df/d10/interface_simulation_view_controller.html#a90ead1604ded7994a7f499f40e5a0edf',1,'SimulationViewController::initializeGestureRecognizer:()'],['../d5/d2f/interface_test_view_controller.html#a76533bea9da475e5ab99ab752d8191f7',1,'TestViewController::initializeGestureRecognizer:()']]],
  ['initializeshader',['initializeShader',['../df/de8/class_main_graphic.html#a4c61ba20c4ba1bab11284bf027d0e721',1,'MainGraphic']]],
  ['insertobject',['insertObject',['../dd/da4/class_main_collision.html#a9d5ce8b8672aa9d3578f173b8c226785',1,'MainCollision::insertObject()'],['../de/da5/class_octree.html#aaf0db997d0dc844a417f3642df1ae19c',1,'Octree::insertObject(OctreeNode *_tree, RigidBody *_body)'],['../de/da5/class_octree.html#a275ff2bcef63ef9859653587d75bcc6f',1,'Octree::insertObject(RigidBody *_body)'],['../df/d96/class_quad_tree.html#abab7faf98530bf5e3847d9f93d30d8ce',1,'QuadTree::insertObject(QuadTreeNode *_tree, RigidBody *_body)'],['../df/d96/class_quad_tree.html#a1ccad8b54111b8162ef4b41667140f28',1,'QuadTree::insertObject(RigidBody *_body)']]],
  ['inverse',['inverse',['../d9/dca/class_matrix3.html#a87d39a4a06b8309ec2a3ae97430dc2ca',1,'Matrix3::inverse()'],['../df/dd7/class_matrix4.html#a9b8f911d61e850810dc199931ace0492',1,'Matrix4::inverse()']]],
  ['invert',['invert',['../d3/de7/class_vector3.html#a189e89cf5fabeefeb9ffe3fdd6db7c1a',1,'Vector3::invert()'],['../d9/dca/class_matrix3.html#ac65c399c2ecb18c7abe099030198e700',1,'Matrix3::invert()'],['../df/dd7/class_matrix4.html#a041c8fdcd90e5b8ca0762f7d3484e5ab',1,'Matrix4::invert()']]],
  ['isawake',['isAwake',['../d6/d6c/class_rigid_body.html#a7cf74b3c037ccbbc8a03673a63f536a2',1,'RigidBody']]],
  ['iscansleep',['isCanSleep',['../d6/d6c/class_rigid_body.html#a4b8b46388122c46a74426471ab01bd8f',1,'RigidBody']]],
  ['iseditmode',['isEditMode',['../d8/d87/class_controller.html#a37e6224f7cb346568b7040c785b77e80',1,'Controller']]],
  ['isinitialized',['isInitialized',['../d8/d87/class_controller.html#a2f1c97c79eedf7dbe8b261c5b033e5d8',1,'Controller::isInitialized()'],['../d7/d4a/class_test_controller.html#ae48a9d660aff1b25c6ec94b845f019a1',1,'TestController::isInitialized()']]],
  ['isrunning',['isRunning',['../d8/d87/class_controller.html#a0de16daecf6d61d78f1de8c796c4300d',1,'Controller::isRunning()'],['../da/d50/class_main_engine.html#a8c35b618ca6c3e260126e5fef073d7b9',1,'MainEngine::isRunning()'],['../d7/d4a/class_test_controller.html#aa290fad22500707202b2b5bc7555ab16',1,'TestController::isRunning()']]],
  ['isselected',['isSelected',['../d7/d6c/class_simulated_object.html#aad258b86e9840463462436ee38dade65',1,'SimulatedObject']]]
];
