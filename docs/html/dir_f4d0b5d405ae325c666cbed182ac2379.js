var dir_f4d0b5d405ae325c666cbed182ac2379 =
[
    [ "ForceGenerators.cpp", "_force_generators_8cpp.html", null ],
    [ "ForceGenerators.h", "_force_generators_8h.html", [
      [ "ForceGenerator", "class_force_generator.html", "class_force_generator" ],
      [ "ForceRegistry", "class_force_registry.html", "class_force_registry" ],
      [ "ForceRegistration", "struct_force_registry_1_1_force_registration.html", "struct_force_registry_1_1_force_registration" ],
      [ "Gravity", "class_gravity.html", "class_gravity" ],
      [ "Spring", "class_spring.html", "class_spring" ]
    ] ],
    [ "LibraryPhysics.h", "_library_physics_8h.html", null ],
    [ "MainPhysics.cpp", "_main_physics_8cpp.html", null ],
    [ "MainPhysics.h", "_main_physics_8h.html", [
      [ "MainPhysics", "class_main_physics.html", "class_main_physics" ]
    ] ],
    [ "ParticleForceGenerators.cpp", "_particle_force_generators_8cpp.html", null ],
    [ "ParticleForceGenerators.h", "_particle_force_generators_8h.html", [
      [ "ParticleForceGenerator", "class_particle_force_generator.html", "class_particle_force_generator" ],
      [ "ParticleForceRegistry", "class_particle_force_registry.html", "class_particle_force_registry" ],
      [ "ParticleForceRegistration", "struct_particle_force_registry_1_1_particle_force_registration.html", "struct_particle_force_registry_1_1_particle_force_registration" ],
      [ "ParticleGravity", "class_particle_gravity.html", "class_particle_gravity" ],
      [ "ParticleDrag", "class_particle_drag.html", "class_particle_drag" ],
      [ "ParticleSpring", "class_particle_spring.html", "class_particle_spring" ],
      [ "ParticleAnchoredSpring", "class_particle_anchored_spring.html", "class_particle_anchored_spring" ],
      [ "ParticleBungee", "class_particle_bungee.html", "class_particle_bungee" ],
      [ "ParticleAnchoredBungee", "class_particle_anchored_bungee.html", "class_particle_anchored_bungee" ],
      [ "ParticleBuoyancy", "class_particle_buoyancy.html", "class_particle_buoyancy" ],
      [ "ParticleFakeStiffSpring", "class_particle_fake_stiff_spring.html", "class_particle_fake_stiff_spring" ]
    ] ],
    [ "TorqueGenerators.cpp", "_torque_generators_8cpp.html", null ],
    [ "TorqueGenerators.h", "_torque_generators_8h.html", [
      [ "TorqueGenerator", "class_torque_generator.html", "class_torque_generator" ]
    ] ]
];