//
//  AppDelegate.h
//  Physical.Simulation.Tool
//
//  Created by Silvio Fragnani da Silva on 11/06/12.
//  
//

#import <UIKit/UIKit.h>
#include "Controller.h"
//!Class responsible for controlling the application states
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
