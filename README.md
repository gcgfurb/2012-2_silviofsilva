About Physical.Simulation.Tool
==============================

    Graphical tool for creating simulations in the area the physics in mobile devices (Apple iOS).

This project is the work of completing bachelor degree in computer science from the University of Blumenau Regional - FURB.

Focus will be to build a physics engine, which can be used for games and simulations. The project focuses on creating a tool for physical simulations.